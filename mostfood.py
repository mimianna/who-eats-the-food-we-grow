#WHich country provides the most food, need to get area abbreviation [1] and unit [8] columns 

# Using Python for this


from pyspark.sql import SparkSession
from pyspark.sql.functions import col, countDistinct,desc
from pyspark import SparkContext, SparkConf
import numpy as np

spark = SparkSession.builder.appName("Most Food By Country").getOrCreate()

#Load the data file
df = spark.read.format('csv').options(header = 'true', inferSchema = 'true').load('file:///home/mshimkovska/project5/cinf401-project-5/FAO.csv')

food = df.groupBy('Area Abbreviation').count()

food2 = food.filter("`count` > 140").sort(desc("count"))

food2.show()

food2.coalesce(1).write.format('com.databricks.spark.csv').options(header = "true").save('file:///home/mshimkovska/project5/cinf401-project-5/outcomeTop16.csv')



