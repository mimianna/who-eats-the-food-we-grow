# Report
# Who eats the food we grow 
##Worldwide food/feed production and distribution, 1961-2013

### Context
###### Our world population is expected to grow from 7.3 billion today to 9.7 billion in the year 2050. Finding solutions for feeding the growing world population has become a hot topic for food and agriculture organizations, entrepreuners and philanthropists. These solutions range from changing the way we grow our food to changing the way we eat it. To make thing harder, the world's climate is changing and it is both affecting and affected by the way we grow our food- agriculture. This dataset provides an insight on our worldwide food production - focusing on a comparison between food produced for human consumption and feed produced for animals. 

### Decision Matrix
![alt text](https://preview.ibb.co/jjkNKS/ANALYSIS.png)

### My reasoning:
###### The dataset was acquired on the delenn server (unix tools) where I saved it and prepared it for analysis
###### My exploratory analysis consisted of loading the datasets into PySpark and aquiring a dataframe from them to be able to manipulate them for my findings.
###### I used RStudio to plot my findings in both a bar plot and a pie chart. I chose the pie chart because it best showed how each data is part of a whole and I chose to use a barplot to show the difference in food growth between the top 16 countries in crop growth
###### I used Spark to distribute my workers to calculate top countries in food growth, percentages of feed (food for livestock) and food (food for people) as well as the number of countries who mainly produce feed and food
###### For sorting my data and getting the outcome I also used Spark
###### I did not use any machine learning or image processing for these tasks

###### Questions to answer:
###### 1. Which country provides the most food(both feed and food)?
###### 2. Percentage of food grown used for livestock vs. the percentage used for human consumption

### Why I chose those questions and dataset:
###### As a vegan I believed that most of the food grown is fed to livestock which then is fed to the richer countries. I am highly interested in anything related to the environment, the way we grow food, and animals. 
###### I chose the dataset because it gave information for these questions from 1961 - 2013 which was the most recent addition to the data.

### Content 
###### The Food and Agriculture Organization of the United Nations provides free access to food and agriculture data for over 245 countries and territories, from the year 1961 to the most recent update (depends on dataset). One dataset from the FAO's database is the Food Balance Sheets. It presents a comprehensive picture of the pattern of a country's food supply during a specific reference period, the last time an update was loaded to the FAO database was in 2013. The food balance sheet shows for each food item the sources of suply and its utilization. This chunk of the dataset is focused on two utilizations of each food item available: 
###### 1.Food  - refers to the total amount of food item available as human food during the reference period. 
###### 2. Feed - refers to the quantity of the food item available for feeding to the livestock and poultry during the reference period. 


### For Question 1:
###### Hypothesis: China and the India will have the largest amount of food grown, considering they are countries with the largest population.
###### Result: The data from FAO shows that China and Tailand are the two nations with the largest food growth, with China leading significantly 

![alt text](https://preview.ibb.co/kUHGDn/justbar.png)
![alt text](https://preview.ibb.co/h0Z7KS/top16.png)

### For question 2
###### My Hypothesis : A significant amount of the food we grow is used as feed for animals and a higher percentage is used to feed animals who will then also become food. 
###### Results : My data found that the amount of food we grow to feed people is actually significantly larger than the the amount we grow for feeding livestock(feed).
![alt text](https://preview.ibb.co/dUfBDn/withcode_and_stuff.png)
![alt text](https://preview.ibb.co/fQN567/justpie.png)

### Conclusion
###### As a conclusion I was right in thinking that China would lead in crop growth. I was surprised to also see Thailand as number two, as I thought it would be India. 
###### For question two I assumed we would feed most of our food to animals but I was surprised to see it went to people. 

### Acknowledgements for the dataset 
###### This dataset was meticulously gathered, organized and published by the Food and Agriculture Organization of the United Nations. 





