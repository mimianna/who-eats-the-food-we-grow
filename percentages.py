#Question 2 through python
# percentages food vs feed


from pyspark.sql import SparkSession
from pyspark.sql.functions import col,countDistinct,rank,sum
from pyspark import SparkContext, SparkConf
import numpy as np

spark = SparkSession.builder.appName("percentages food vs feed").getOrCreate()

#Load data
df = spark.read.format('csv').options(header = "true", inferSchema ="true").load('file:///home/mshimkovska/project5/cinf401-project-5/FAO.csv')

percentage = df.groupBy('Element').count()

total = percentage.select('count').agg({"count": "sum"}).collect().pop()['sum(count)']

result = percentage.withColumn('percent', (percentage['count']/total)*100)
result.show()


#withColumn('Percent', col('count')*100/sum(col('count')))

#percentage.show()



#percentage.coalesce(1).write.format('com.databricks.spark.csv').options(header = 'true').save('file:///home/mshimkovska/project5/cinf401-project-5/percentageOutcome.csv')
