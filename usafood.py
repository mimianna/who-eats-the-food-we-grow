# USA feed and food 
# China feed and food 

from pyspark.sql import SparkSession
from pyspark import SparkContext, SparkConf
from pyspark.sql.functions import countDistinct
import numpy as np

spark = SparkSession.builder.appName("USA feed vs food").getOrCreate()

df = spark.read.format("csv").options(header ="true", inferSchema = 'true').load('file:///home/mshimkovska/project5/cinf401-project-5/FAO.csv')

usa = df.select("Area Abbreviation")
print(usa.take(3))
